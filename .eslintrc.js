module.exports = {
  'root': true,

  'extends': [
    'eslint:recommended',
    'plugin:react/recommended'
  ],

  'plugins': ['react', 'react-hooks'],

  'parserOptions': {
    ecmaVersion: 2017,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      experimentalObjectRestSpread: true
    }
  },

  'env': {
    browser: true,
    node: true
  },


  'rules': {
    // Possible errors
    'no-extra-parens': [
      'warn',
      'all',
      { nestedBinaryExpressions: false, ignoreJSX: 'multi-line' }
    ],
    'no-import-assign': 'error',
    'no-template-curly-in-string': 'error',

    // Best practices
    'array-callback-return': 'error',
    'curly': ['error', 'multi-line'],
    'default-param-last': 'warn',
    'dot-location': ['warn', 'property'],
    'dot-notation': 'warn',
    'eqeqeq': ['error', 'always', { 'null': 'ignore' }],
    'no-eval': 'error',
    'no-extend-native': 'error',
    'no-extra-bind': 'warn',
    'no-floating-decimal': 'warn',
    'no-implied-eval': 'error',
    'no-invalid-this': 'error',
    'no-lone-blocks': 'warn',
    'no-loop-func': 'error',
    'no-multi-spaces': 'warn',
    'no-new-wrappers': 'warn',
    'no-proto': 'error',
    'no-return-await': 'error',
    'no-sequences': 'error',
    'no-throw-literal': 'error',
    'no-unmodified-loop-condition': 'error',
    'no-unused-expressions': [
      'error',
      { allowShortCircuit: true, allowTernary: true }
    ],
    'no-useless-call': 'error',
    'no-useless-concat': 'warn',
    'no-useless-return': 'warn',
    'no-void': 'error',
    'prefer-regex-literals': 'error',
    'require-await': 'error',
    'wrap-iife': ['error', 'inside'],

    // Variables
    'no-undef-init': 'warn',
    'no-unused-vars': 'warn',
    'no-use-before-define': 'error',

    // Stylistic issues
    'array-bracket-newline': ['warn', 'consistent'],
    'array-bracket-spacing': 'warn',
    'block-spacing': 'warn',
    'brace-style': 'warn',
    'comma-dangle': 'warn',
    'comma-spacing': 'warn',
    'comma-style': 'warn',
    'computed-property-spacing': 'warn',
    'consistent-this': 'warn',
    'eol-last': 'warn',
    'func-call-spacing': 'warn',
    'func-name-matching': 'warn',
    'indent': ['warn', 2, { SwitchCase: 1 }],
    'jsx-quotes': 'warn',
    'key-spacing': 'warn',
    'keyword-spacing': 'warn',
    'linebreak-style': 'error',
    'lines-between-class-members': 'warn',
    'multiline-ternary': ['warn', 'always-multiline'],
    'new-cap': 'warn',
    'new-parens': 'warn',
    'no-array-constructor': 'warn',
    'no-bitwise': 'error',
    'no-lonely-if': 'warn',
    'no-mixed-operators': 'warn',
    'no-multiple-empty-lines': ['warn', { max: 2, maxBOF: 0, maxEOF: 0 }],
    'no-negated-condition': 'warn',
    'no-nested-ternary': 'warn',
    'no-new-object': 'warn',
    'no-tabs': 'warn',
    'no-trailing-spaces': 'warn',
    'no-unneeded-ternary': 'warn',
    'no-whitespace-before-property': 'warn',
    'nonblock-statement-body-position': 'error',
    'object-curly-newline': 'warn',
    'object-curly-spacing': ['warn', 'always'],
    'one-var': ['warn', 'never'],
    'operator-assignment': 'warn',
    'operator-linebreak': [
      'warn',
      'after',
      { overrides: { '?': 'before', ':': 'before' } }
    ],
    'padded-blocks': ['warn', { classes: 'always' }],
    'prefer-object-spread': 'warn',
    'quote-props': ['warn', 'consistent-as-needed', { keywords: true }],
    'quotes': ['warn', 'single', { avoidEscape: true }],
    'semi': 'error',
    'semi-spacing': 'warn',
    'semi-style': 'warn',
    'space-before-blocks': 'warn',
    'space-before-function-paren': [
      'warn',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always'
      }
    ],
    'space-in-parens': 'warn',
    'space-infix-ops': 'warn',
    'space-unary-ops': ['warn'],
    'spaced-comment': 'warn',
    'switch-colon-spacing': 'warn',

    // ECMAScript 6
    'arrow-body-style': 'warn',
    'arrow-parens': 'warn',
    'arrow-spacing': 'warn',
    'no-duplicate-imports': 'warn',
    'no-useless-computed-key': 'warn',
    'no-useless-constructor': 'warn',
    'no-useless-rename': 'warn',
    'no-var': 'warn',
    'object-shorthand': 'warn',
    'prefer-arrow-callback': 'warn',
    'prefer-rest-params': 'warn',
    'prefer-spread': 'warn',
    'prefer-template': 'warn',
    'rest-spread-spacing': 'warn',
    'template-curly-spacing': 'warn',

    // Undo plugin:react/recommended
    'react/prop-types': 'off',

    // React rules
    // 'react/button-has-type': 'error',
    'react/no-access-state-in-setstate': 'error',
    'react/no-array-index-key': 'warn',
    'react/no-multi-comp': ['warn', { ignoreStateless: true }],
    'react/no-redundant-should-component-update': 'warn',
    'react/no-typos': 'error',
    'react/no-this-in-sfc': 'error',
    'react/no-unsafe': 'error',
    'react/no-unused-prop-types': 'error',
    'react/no-unused-state': 'error',
    'react/no-will-update-set-state': ['error', 'disallow-in-func'],
    'react/prefer-es6-class': 'warn',
    'react/prefer-stateless-function': ['warn', { ignorePureComponents: true }],
    'react/self-closing-comp': 'warn',
    'react/sort-comp': 'warn',
    'react/style-prop-object': 'warn',
    'react/display-name': 'off',
    'react/no-unescaped-entities': 'off',

    // JSX-specific rules
    'react/jsx-closing-bracket-location': ['warn', 'line-aligned'],
    'react/jsx-closing-tag-location': 'warn',
    'react/jsx-curly-spacing': 'warn',
    'react/jsx-equals-spacing': 'warn',
    // 'react/jsx-filename-extension': 'warn',
    'react/jsx-first-prop-new-line': 'warn',
    // 'react/jsx-handler-names': 'warn',
    'react/jsx-indent': [
      'warn',
      2,
      { checkAttributes: true, indentLogicalExpressions: true }
    ],
    'react/jsx-indent-props': ['warn', 2],
    // 'react/jsx-no-bind': 'warn',
    'react/jsx-curly-brace-presence': 'warn',
    // 'react/jsx-curly-newline': 'warn',
    'react/jsx-fragments': 'warn',
    'react/jsx-pascal-case': 'warn',
    'react/jsx-props-no-multi-spaces': 'warn',
    // 'react/jsx-sort-props': ['warn', { ignoreCase: true, callbacksLast: true, shorthandLast: true, reservedFirst: true }],
    'react/jsx-tag-spacing': ['warn', { beforeClosing: 'never' }],

    // React Hooks rules
    'react-hooks/rules-of-hooks': 'warn',
    'react-hooks/exhaustive-deps': 'warn',

    // React 17 doesn't need these
    'react/jsx-uses-react': 'off',
    'react/react-in-jsx-scope': 'off'

  }
};
