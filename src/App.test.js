import { render, screen } from '@testing-library/react';
import App from './App';

describe('checks if App.js runs', () => {
  test('renders get weather data button', () => {
    render(<App />);
    const linkElement = screen.getByText(/Wonder what the weather is like/i);
    expect(linkElement).toBeInTheDocument();
  });
});
