import styled from 'styled-components';

const Button = ({ text, onClick, className }) =>
  <button className={className} onClick={onClick}>
    <div>{text}</div>
  </button>;

const StyledButton = styled(Button)`
  padding: 1em 2em;
  border-radius: 5px;
  background-color: ghostwhite;
`;

export default StyledButton;
