import styled from 'styled-components';

const Icon = ({ icon, className }) =>
  <img className={className} src={`${process.env.PUBLIC_URL}/icons/${icon}.svg`} alt={icon} />;


const StyledIcon = styled(Icon)`
  width: 150px;
  height: 150px;
`;

export default StyledIcon;
