import styled from 'styled-components';

const Text = ({ text, className }) =>
  <span className={className}>{text}</span>;

const StyledText = styled(Text)`
  padding-bottom:1em
`;

export default StyledText;
