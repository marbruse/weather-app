import './App.css';
import { useEffect, useState } from 'react';
import Button from './components/Button';
import Spinner from './components/Spinner';
import Icon from './components/Icon';
import Text from './components/Text';

const App = () => {
  const [position, setPosition] = useState(false);
  const [weather, setWeather] = useState({ loading: false, data: null });

  const getWeatherData = (position) => {
    fetch(`https://api.met.no/weatherapi/locationforecast/2.0/complete?lat=${position.latitude}&lon=${position.longitude}`)
      .then((response) => {
        response.json().then((data) => {
          setWeather({ loading: false, data });
        });
      }).catch((err) => {
        console.log('error', err);
      });
  };

  const getCoordinates = () => {
    if (navigator.geolocation) {
      setWeather({ loading: true, data: null });
      navigator.geolocation.getCurrentPosition((position) =>
        setPosition(position.coords)
      );
    }
  };

  useEffect(() => {
    if (position) getWeatherData(position);
  }, [position]);

  return (
    <div className="App">
      <header className="App-header">
        {weather.data &&
          <>
            <Icon icon={weather.data.properties.timeseries[0].data.next_1_hours.summary.symbol_code} />
            <Text
              text={`Temperature: ${weather.data.properties.timeseries[0].data.instant.details.air_temperature}`}
            />
          </>
        }
        {weather.loading && <Spinner />}
        <Button
          text="Wonder what the weather is like..."
          onClick={() => getCoordinates()}
        />
      </header>
    </div>
  );
};

export default App;
